"use strict"

var gulp = require('gulp');
var concatCss = require('gulp-concat-css'),
    rename = require('gulp-rename'),
    minifyCSS = require('gulp-minify-css'),
    autoprefixer = require('gulp-autoprefixer'),
    livereload = require('gulp-livereload'),
    connect = require('gulp-connect'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    concat = require('gulp-concat'),
    plumber = require('gulp-plumber'),
    order = require('gulp-order'),
    uglify = require('gulp-uglify'),
    notify = require('gulp-notify');


gulp.task('connect', function() {
    connect.server({
        root: './',
        livereload: true
    });
});


gulp.task('css', function () {
    return gulp.src([
        'bower_components/font-awesome/css/font-awesome.css',
        'bower_components/css-reset/reset.css',
        'src/css/main.css'
    ])
        .pipe(plumber())
        .pipe(autoprefixer('last 3 versions', '> 2%', 'ie 9'))
        .pipe(concat('main.css'))
        .pipe(gulp.dest('./css/'))
        //.pipe(minifyCSS())
        //.pipe(rename('main.min.css'))
        .pipe(gulp.dest('./css/'))
        .pipe(connect.reload())
        // .pipe(notify('CSS Done!'));
});


gulp.task('html', function() {
    //return gulp.src('src/pug/catalog-l2.pug')
    return gulp.src('src/html/*.html')
        .pipe(plumber())
        .pipe(gulp.dest('./'))
        .pipe(connect.reload())
        // .pipe(notify('HTML Done!'));
});

gulp.task('fonts', function() {
    return gulp.src('src/vendors/roboto-googlefont/*.ttf')
        .pipe(gulp.dest('./fonts/'))
        .pipe(connect.reload())
        // .pipe(notify('Fonts moved!'));
});


gulp.task('img', function() {
    return gulp.src('src/img/**/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('./img'))
        .pipe(connect.reload())
        // .pipe(notify('IMG Done!'));
});

gulp.task('scripts', function() {
    return gulp.src([
        'src/js/*.js'
    ])
        .pipe(plumber())
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./js/'))
        //.pipe(uglify())
        //.pipe(rename('main.min.js'))
        .pipe(gulp.dest('./js/'))        
        .pipe(connect.reload())
        // .pipe(notify('JS Done!'));
});


gulp.task('watch', function() {
    gulp.watch('src/css/**/*.css', ['css'])
    gulp.watch('src/html/**/*.html', ['html'])
    gulp.watch('src/img/**/*', ['img'])
    gulp.watch('src/js/**/*.js', ['scripts'])
});

gulp.task('default', ['connect', 'html', 'css', 'img', 'scripts', 'fonts', 'watch']);
